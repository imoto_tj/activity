<?php

declare(strict_types=1);

namespace App\Http\Procedures;

use App\Models\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Sajya\Server\Procedure;

class ActivityProcedure extends Procedure
{

    public static string $name = 'activity';


    public function store(Request $request)
    {
        $request->validate([
            'url' => 'required|string|max:255',
            'st_date' => 'required|string|max:255'
        ]);
        $save = Activity::create($request->all());
        if (!$save){
            return 'error while saving data';
        }
        return 'ok';
    }

    public function getdata(){

        return DB::table('activities')
            ->select('url', DB::raw('count(*) as total'),DB::raw('max(st_date) as st_date'))
            ->groupBy('url')
            ->get();

    }
}
